# Лабораторная работа 1. «Сбор данных социальных сетей»

[назад](README.md)

В целях практического применения описанных в пособии сведений, а также овладения существующим инструментарием в области анализа социальных сетей в данном учебном пособии авторами приводятся следующие четыре лабораторные работы.

При выполнении данной лабораторной работы приобретаются навыки взаимодействия с открытыми API социальных сетей VKонтакте и Twitter.

Для выполнения данной лабораторной работы необходимы аккаунты в этих социальных сетях.

## 1. Инсталляция и настройка Python окружения
Лабораторная работа выполняется с использованием языка программирования Python. В случае отсутствия данного программного обеспечения на компьютере необходимо перейти на
официальный сайт и скачать последнюю версию Python. Далее установить Python на компьютер, следуя указаниям мастера установки.

Для выполнения также необходимы пакеты `vk_api` и `tweepy`. Для установки этих пакетов необходимо выполнить в командной строке с правами администратора команду вида:
```
pip install <имя_пакета>
```
## 2. Подключение к социальной сети VK

Необходимо открыть Python Shell и запустить к исполнению следующий код.

```python
import vk_api

def auth_handler():
  """ При двухфакторной аутентификации вызывается
эта функция.
"""
  # Код двухфакторной аутентификации
  key = input("Enter authentication code: ")
  # Если: True - сохранить, False - не сохранять.
  remember_device = True
  return key, remember_device

def stop_f(items):
  print (items)
    
def main():
  login, password = '<ВАШ ЛОГИН>', '<ВАШ ПАРОЛЬ>'
  vk_session = vk_api.VkApi(
    login, password,
    auth_handler=auth_handler  # функция для обработки двухфакторной аутентификации
  )

  try:
      vk_session.auth()
  except vk_api.AuthError as error_msg:
      print(error_msg)

  tools = vk_api.VkTools(vk_session)
  vk_app = vk_session.get_api()
  print(vk_app.wall.post(message='Hello world!'))

if __name__ == '__main__':
  main()
```

Этот код отправит запрос на авторизацию в VK (для получения доступа к Вашей учётной записи) и опубликует на Вашей странице запись от Вашего имени с содержанием «Hello world!».

Далее можно попробовать скачать все посты (записи) на странице сообщества. Для этого нужно выбрать произвольное сообщество (к примеру, группу [«Аспирантов Самарского университета»](https://vk.com/ssau_asp)) и выполнить следующий код:

```python
import vk_api
import json
group_id = -43938013

def main():
  """ Пример получения всех постов со стены """
  login, password = '<ВАШ ЛОГИН>', '<ВАШ ПАРОЛЬ>'
  vk_session = vk_api.VkApi(login, password)
  try:
      vk_session.auth(token_only=True)
  except vk_api.AuthError as error_msg:
      print(error_msg)
      return
  tools = vk_api.VkTools(vk_session)
  wall = tools.get_all('wall.get', 100,
{'owner_id': group_id})
  print('Posts count:', wall['count'])
  with open(r" wall_asp.json", 'a') as f:
    f.write(json.dumps(wall))
  
if __name__ == '__main__':
  main()
```

В файле `wall_asp.json` будет находиться дамп всех сообщений на стене выбранного сообщества (в нашем примере, группа [«Аспирантов Самарского университета»](https://vk.com/ssau_asp)). Можно увидеть, в полученном файле достаточно много непонятных слов и цифр. Это различные служебные и информационные поля. В них содержится информация о том, когда был сделан пост, кем, сколько лайков, сколько комментариев и многое.

Для изучения `json` из терминала удобным инструментом является https://www.visidata.org/, который может быть установлен командой `pip install visidata`.

У Api социальной сети VKонтакте много возможностей. Подробнее о них можно прочитать на странице документации (https://vk.com/dev/manuals).

## 3. Подключение к социальной сети Twitter

Алгоритм подключения к Twitter практически не отличается от подключения к социальной сети VKонтакте.

Для начала необходимо получить секретный токен. Для этого нужно перейти по ссылке (https://developer.twitter.com/en/apps) и нажать кнопку `Create an app`. После ввода всех требуемых данных, приложение сгенерирует API key и API key (рис. 29). Также стоит поменять права доступа приложения на чтение и запись.

![Рис. 29. Создание приложения по подключению к Twitter API](images/2021-09-04_16-19-31.png)  
Рис. 29. Создание приложения по подключению к Twitter API

Затем следует подключиться к социальной сети Twitter и запросить информацию о количестве подписчиков пользователя данной сети.

```python      
import tweepy
API_KEY = "YOUR_API_KEY"
API_SECRET = "YOUR_API_SECRET"
ACCESS_TOKEN = "YOUR_ACCESS_TOKEN"
ACCESS_TOKEN_SECRET = "YOUR_ACCESS_TOKEN_SECRET"
auth = tweepy.OAuthHandler(API_KEY, API_SECRET)
auth.set_access_token(ACCESS_TOKEN,
ACCESS_TOKEN_SECRET)
api = tweepy.API(auth)
user = api.get_user('twitter')
print(user.screen_name, user.followers_count)
```

Данный код вызывает метод API, который называется get_user и возвращает информацию о пользователе, имя которого мы указали (в примере – 'twitter'). Можно указать другое имя (своё, например) и посмотреть результат. Затем нужно вывести `user.screen_name` и `user.followers_count` – это отображаемое имя и количество тех, кто подписался на данного пользователя.

Далее попробуем что-либо написать на своей странице. Для этого запустим на исполнение следующий код:

```python
import tweepy
API_KEY = "YOUR_API_KEY"
API_SECRET = "YOUR_API_SECRET"
ACCESS_TOKEN = "YOUR_ACCESS_TOKEN"
ACCESS_TOKEN_SECRET = "YOUR_ACCESS_TOKEN_SECRET"
auth = tweepy.OAuthHandler(API_KEY, API_SECRET)
auth.set_access_token(ACCESS_TOKEN,
ACCESS_TOKEN_SECRET)
api = tweepy.API(auth)
api.update_status('Просто так твит ни о чём!')
```

В результате получается следующее (рис. 30).

![Рис. 30. Вывод сообщения в результате исполнения кода](images/2021-09-04_16-22-12.png)  
Рис. 30. Вывод сообщения в результате исполнения кода

Подробнее про Twitter API можно прочитать по ссылке (https://developer.twitter.com/en/docs/api-reference-index).

Для сбора данных подключимся к StreamingaAPI следующим образом.
```python
import tweepy

API_KEY = "YOUR_API_KEY"
API_SECRET = "YOUR_API_SECRET"
ACCESS_TOKEN = "YOUR_ACCESS_TOKEN"
ACCESS_TOKEN_SECRET = "YOUR_ACCESS_TOKEN_SECRET"
GEOBOX_SAMARA_BIG = [48.9700523344,52.7652295668,50.7251182524,53.6648329274]

class MyStreamListener(tweepy.StreamListener):
  def on_status(self, status):
    with open('messages.txt' , 'a') as f:
      f.write ("@{}, сообщение номер {}\n {}\n".format(
        status.author.screen_name,
        status.id_str,
        status.text)
      )
    
    print("@{}, сообщение номер {}\n {}\n".format(
      status.author.screen_name,
      status.id_str,
      status.text)
    )

  def on_error(self, status_code):
    if status_code == 420:
      return False

def monitoring_tweets(api, query="", locations=None):
  myStreamListener = MyStreamListener()
  myStream = tweepy.Stream(
    auth = api.auth,
    listener=myStreamListener
  )
  myStream.filter(track=[query], locations=locations)

if __name__ == '__main__':
  while True:
    try:
      auth = tweepy.OAuthHandler(API_KEY, API_SECRET)
      auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
      api = tweepy.API(auth)
      monitoring_tweets(api, query="", locations=GEOBOX_SAMARA_BIG)
    except Exception as error_msg:
      print (error_msg)
```

Данный код обращается к серверам Twitter с запросом всех сообщений, у которых точка отправки сообщения находится в пределах указанного геобокса (`GEOBOX_SAMARA_BIG`).


Подробнее о StreamingAPI можно почитать здесь (https://developer.twitter.com/en/docs/tutorials/consuming-streaming-data.html).
