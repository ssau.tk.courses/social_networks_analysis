# БИБЛИОГРАФИЧЕСКИЙ СПИСОК 

[назад](README.md)

1. Aizawa A. An information-theoretic perspective of tf–idf measures // Information Processing & Management. 2003. Т. 39. №1. P. 45-65. 
2. Bakaev V. A., Blagov A. V. The analysis of profiles on social networks // Information Technologies and Nanotechnologies. 2018. P. 1860-1863. 
3. Barabasi A.L., Bonabeau E. Scale Free Networks // Scientific American, 2003, P. 50-59. 
4. Blagov A. et al. Big data instruments for social media analysis // Proceedings of the 5th International Workshop on Computer Science and  Engineering. 2015. P. 179-184. 
5. Dean J., Ghemawat S. MapReduce: simplified data processing on large clusters // Communications of the ACM. 2008. Т.51. №1. P. 107-113. 
6. Key Trends to Watch in Gartner 2012 Emerging Technologies Hype Cycle. URL: http://www.forbes.com/sites/gartnergroup/2012/09/18/key-trends-to-watch-in-gartner-2012-emerging-technologies-hype-cycle-2. 
7. Leskovec J., Faloutsos C. Sampling from large graphs // Proceedings of the 12th ACM SIGKDD international conference on Knowledge discovery and data mining. ACM, 2006. P. 631-636. 
8. Liu X., Murata T. Advanced modularity-specialized label propagation algorithm for detecting communities in networks // Physica A: Statistical Mechanics and its Applications, Vol. 389, Issue 7, P. 1493-1500.  
9. Newman, M. E. J. Fast algorithm for detecting community structure in networks // Phys. Rev. E. 2004. T.69. P. 576-587. 
10. Newman M. E. J., Girvan M. Finding and evaluating community structure in networks // Phys. Rev. E. 2004. T. 69. P. 426-438.
11. Rytsarev I. A., Blagov A. V. Classification of Text Data from the Social Network Twitter // CEUR Workshop Proceedings. 2016. Т. 1638. P. 851-856. 
12. Social-network-sourced big data analytics / W. Tan, M.W. Blake, I. Saleh, S. Dustdar // IEEE Internet Computing. 2013. я. 5. P. 62-69. 
13. VanBoskirk S., Overby C. S., Takvorian S. US interactive marketing forecast, 2011 to 2016 // Forrester Research. 2011.
14. Xu X. Scan: a structural clustering algorithm for networks // Proceedings of the 13th ACM SIGKDD international conference on Knowledge discovery and data mining. ACM, 2007. P. 824-833. 
15. Венгерский алгоритм решения задачи о назначениях. URL: http://e-maxx.ru/algo/assignment_hungary. 
16. Гусарова Н. Ф. Анализ социальных сетей. Основные понятия и метрики // Университет ИТМО. СПб., 2016. 70 c. 
17. Анализ социальных сетей: методы и приложения / А. Коршунов [и др.] // Труды Института системного программирования РАН. 2014. Т. 26. №. 1. 
18. Красников И. А., Никуличев Н. Н. Гибридный алгоритм классификации текстовых документов на основе анализа внутренней связности текста // ИВД. 2013. № 3(26). 
19. Сметанин Н. Нечёткий поиск в тексте и словаре. URL: https://habrahabr.ru/post/114997.  
20. Рекомендательные системы: LDA. 2014. URL: https://habrahabr.ru/company/surfingbird/blog/150607. 
21. Хотилин М. И., Благов А. В. Визуальное представление и кластерный анализ социальных сетей // Материалы Международной конференции ИТНТ. Самара, 2016. С. 1067-1072.

[назад](README.md)